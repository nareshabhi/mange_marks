import { Component } from '@angular/core';

interface IStudent {
  name: string,
  exams: Array<IExam>
}
interface IExam {
  subjectName: string,
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'studen';

  students:Array<IStudent>=[];

  student:IStudent = {
      name: '',
      exams:[]
  }

  exam:IExam= {
    subjectName: ''
  }

  addStudent(){
    let student = JSON.parse(JSON.stringify(this.student));
    this.students.push(student);
  }
  
  addExams(index){
    let exams = JSON.parse(JSON.stringify(this.exam));
   this.students[index].exams.push(exams)


  }

}
